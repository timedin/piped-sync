# Piped-Sync

A sync-server for Piped.

## Features
- [x] Manually Sync Subscriptions 
    - [x] (from one instances server to multiple)  
    - [ ] (use multiple getter Instances) 
- [ ] Sync Playlists
- [ ] Automatic schedule sync
- [ ] Multiple User Support

## Install
- Install a webserver with php>=8.0
    - php-mysqli, php-curl
- Create a new Database
    - Import both db-setup .sql files
    - (you can add more piped instances)
- set your Database Credententials in config/config.ini
- restrict access to config/config.ini (!Important) and data-handler

## Disclaimer
- Code is messy, unclean and has security flaws
- Sync of large amount of data may slow down your webserver (Requests are not async optimized)