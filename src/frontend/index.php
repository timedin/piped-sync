<?php
    $userID = 1;
?><?php
require_once(__DIR__."/../../res/setup.php");

if(isset($_POST["save-sync-status"])) {
    require_once(__DIR__."/../data-handler/save-settings.php");
    require_once(__DIR__."/../data-handler/home.php");
} else if(isset($_POST["auth-instance"])||isset($_POST["auth-instance-login"])) {
    require_once(__DIR__."/../data-handler/login-instance.php");
} else if(isset($_POST["getSubs"])) {
    header("Refresh: 2;");
    require_once(__DIR__."/../data-handler/getSubs.php");
} else if(isset($_POST["setSubs"])) {
    header("Refresh: 2;");
    require_once(__DIR__."/../data-handler/setSubs.php");
} else if(isset($_POST["getPlaylists"])) {
    header("Refresh: 2;");
    require_once(__DIR__."/../data-handler/getPlaylists.php");
} else if(isset($_POST["setPlaylists"])) {
    header("Refresh: 2;");
    require_once(__DIR__."/../data-handler/setSubs.php");
} else {
    require_once(__DIR__."/../data-handler/home.php");
}
?>