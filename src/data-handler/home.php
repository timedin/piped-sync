<?php
$subscriptions = $DBManager->query(<<<EOD
SELECT channel_id,name
FROM subscriptions
WHERE user_id="%a0";
EOD,$userID)??array();
$authedInstances = $DBManager->query(<<<EOD
SELECT id,name, (sync_table.type = "set") as sync_active, sync_table.type 
FROM instances
LEFT JOIN sync_table
ON instances.id = sync_table.instance_id
LEFT JOIN auth
ON auth.instance_id=instances.id
WHERE auth.user_id = "%a0";
EOD,$userID)??array();
$notUsedInstances = $DBManager->query(<<<EOD
SELECT *
FROM instances
LEFT JOIN auth
ON auth.instance_id=instances.id
WHERE auth.user_id is NULL;
EOD,$userID)??array();
?>
<html>
    <head>
        <style>
            .instance-is-authed {
                background-color: limegreen;
            }
            #loggedin-view {
                width: 30rem;
                height: 5rem;
                border: thin solid black;
                overflow-y: scroll;
            }
        </style>    
</head>
    <body>
        <h1>Your Piped Sync</h1>
        <p>UserID: <span><?php echo $userID;?></span></p>
        <form method="POST">
            <input type="submit" name="getSubs" value="Get Subscriptions">    
        </form>
        <form method="POST">
            <input type="submit" name="setSubs" value="Set Subscriptions">    
        </form>
        <h2>Subscriptions</h2>
        
        <select id="sub-view" size="<?php echo min(count($subscriptions),10);?>">
            <?php
            foreach($subscriptions as $sub) {
                echo "<option value=\"{$sub["channel_id"]}\">{$sub["name"]}</option>";
            }
            ?>
        </select>
        <h2>Instances</h2>
        <h3>Authed</h3>
        <form action="" method="POST">
            <h4>Sync From</h4>
            <select name="getter-inst" autocomplete="off">
                <?php
                foreach($authedInstances as $inst) {
                    $selected = ($inst["type"]=="get")?"selected":"";

                    echo "<option {$selected} value=\"inst-{$inst["id"]}\">{$inst["name"]}</option>";
                }
                ?>
            </select>
            <h4>Sync To</h4>
            <div id="loggedin-view">
                <?php
                foreach($authedInstances as $inst) {
                    $checked = $inst["sync_active"]?"checked":"";
                    echo "<div><input autocomplete=\"off\" type=\"checkbox\" name=\"inst-{$inst["id"]}\" id=\"check-inst-{$inst["id"]}\" {$checked}><label for=\"check-inst-{$inst["id"]}\">{$inst["name"]}</label></div>";
                }
                ?>
            </div>
            <input type="submit" name="save-sync-status" value="Save settings.">
        </form>
        <h3>Other</h3>
        <form action="" method="POST">
            <select id="instance-view" name="instance-auth" size="<?php echo min(count($notUsedInstances),10);?>">
                <?php
                foreach($notUsedInstances as $inst) {
                    $isAuth = ($inst["auth_token"]=NULL)?"instance-is-authed":"";
                    echo "<option class=\"{$isAuth}\" value=\"{$inst["id"]}\">{$inst["name"]}</option>";
                }
                ?>
            </select>
            <input type="submit" name="auth-instance" value="Login to Instance">
        </form>
    </body>
</html>