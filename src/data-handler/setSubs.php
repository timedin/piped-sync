<?php
$instances = $DBManager->query(<<<EOD
SELECT id, name, api_url, auth_token
FROM instances
INNER JOIN sync_table
ON sync_table.instance_id=instances.id
INNER JOIN auth
ON sync_table.instance_id=auth.instance_id AND auth.user_id=sync_table.user_id
WHERE sync_table.type="set" AND sync_table.user_id="%a0";
EOD,$userID);

$subscriptions = $DBManager->query(<<<EOD
SELECT channel_id
FROM subscriptions
WHERE user_id="%a0";
EOD,$userID);

$subs_data = array();
foreach($subscriptions as $sub) {
    array_push($subs_data,$sub["channel_id"]);
}

foreach($instances as $instance) {
    $token = $instance["auth_token"];
    $options = [
        CURLOPT_POST=>1,
        CURLOPT_POSTFIELDS=>json_encode($subs_data),
        CURLOPT_RETURNTRANSFER=>true,
        CURLOPT_HTTPHEADER=> ["Authorization: {$token}","User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:109.0) Gecko/20100101 Firefox/111.0"],    
    ];
    $curl = curl_init("https://{$instance["api_url"]}/import?override=true");
    
    curl_setopt_array($curl,$options);
    $resp = curl_exec($curl);
    $aResp=json_decode($resp,true);
    if($aResp==null) {
        echo("Sync to {$instance["name"]} failed: {$resp}");
    } else if(array_key_exists("error",$aResp)) {
        echo("Sync to {$instance["name"]} failed: {$aResp["error"]}");
    } else {
        echo("Sync to {$instance["name"]} succesfull.");
    }
}
?>