<?php
if(isset($_POST["auth-instance-login"])) {
    $iID=$_POST["instance"];
    $instance = $DBManager->QuerySingleRow("SELECT id,name,api_url FROM instances WHERE id=\"{$iID}\";");
    $options = [
        CURLOPT_HTTPHEADER=>["User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:109.0) Gecko/20100101 Firefox/111.0"],
        CURLOPT_POST => 1,
        CURLOPT_POSTFIELDS => "{\"username\":\"{$_POST["username"]}\",\"password\":\"{$_POST["passwd"]}\"}",
        CURLOPT_RETURNTRANSFER=>true
    ];
    $curl = curl_init("https://{$instance["api_url"]}/login");
    curl_setopt_array($curl,$options);
    $resp = curl_exec($curl);
    curl_close($curl);
    $jResp = json_decode($resp,true);
    if(array_key_exists("error",$jResp)) {
        header("Refresh: 2;");
        die($jResp["error"]);
    }

    $token = $jResp["token"];

    $DBManager->update(<<<EOD
    INSERT IGNORE INTO auth (`user_id`,`auth_token`,`instance_id`) 
    VALUES ({$userID},"%a0",{$iID}) 
    ON DUPLICATE KEY 
    UPDATE `auth_token`="%a0";
    EOD,$token);
    header("Refresh: 2;");
    die("login success");
}
?>
<form action="" method="POST">
    <p><?php echo $DBManager->QuerySingleRow("SELECT id,name,api_url FROM instances WHERE id=\"{$_POST["instance-auth"]}\";")["id"];?></p>
    <label for="userIn">Username</label>
    <input type="text" name="username" id="userIn">
    <input type="hidden" name="instance" value="<?php echo $_POST["instance-auth"];?>">
    <label for="passwdIn">Password</label>
    <input type="password" name="passwd" id="passwdIn">
    <input type="submit" name="auth-instance-login" value="Submit">
</form>

