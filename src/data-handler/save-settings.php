<?php
$DBManager->delete(<<<EOD
DELETE 
FROM sync_table 
WHERE user_id = "%a0";
EOD,$userID);

$sql = "INSERT INTO sync_table (user_id, instance_id, type) VALUES ";
$runs = false;
foreach($_POST as $key => $post) {
    if(str_starts_with($key,"inst-")) {
        $runs = true;
        $cID = substr($key,5);
        $sql.="(\"{$userID}\", \"{$cID}\", \"set\"),"; 
    }
}
if($runs) {
    $sql[strlen($sql)-1]=";";
    $DBManager->insert($sql);
}
$DBManager->update(<<<EOD
INSERT INTO `sync_table` (`user_id`, `instance_id`, `type`) 
VALUES ("%a0", "%a1", "get")
ON DUPLICATE KEY
UPDATE type="get";
EOD,$userID,substr($_POST["getter-inst"],5));
?>