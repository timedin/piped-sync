<?php
$DBManager->delete(<<<EOD
DELETE 
FROM `subscriptions` 
WHERE `user_id` = "%a0";
EOD,$userID);

$instance = $DBManager->QuerySingleRow(<<<EOD
SELECT id, name, api_url, auth_token
FROM instances
INNER JOIN sync_table
ON sync_table.instance_id=instances.id
INNER JOIN auth
ON sync_table.instance_id=auth.instance_id AND auth.user_id=sync_table.user_id
WHERE sync_table.type="get" AND sync_table.user_id="%a0";
EOD,$userID);

$token = $instance["auth_token"];

$options = [
    CURLOPT_HTTPHEADER=> ["Authorization: {$token}","User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:109.0) Gecko/20100101 Firefox/111.0"],    
    CURLOPT_RETURNTRANSFER=>true
];
$curl = curl_init("https://{$instance["api_url"]}/subscriptions");
curl_setopt_array($curl,$options);
$resp = curl_exec($curl);
$aResp=json_decode($resp,true);
$sql = "INSERT INTO `subscriptions` (`id`, `channel_id`, `user_id`, `name`) VALUES ";
foreach($aResp as $sub) {
    $cID = substr($sub["url"],9);
    echo "<p>{$cID}:{$sub["name"]}</p>";
    $sql.="(NULL, \"{$cID}\", {$userID}, \"{$sub["name"]}\"),";
}
$sql[strlen($sql)-1]="O";
$sql.="N DUPLICATE KEY 
UPDATE user_id={$userID}";
$DBManager->insert($sql);
?>